"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

from setuptools import setup, find_packages

with open('README.md','r') as f:
    readme = f.read()

setup(name='Tellurium',
      description=readme,
      author='Davis Marques',
      url='https://bitbucket.org/esrc/tellurium',
      version='0.1.0',
      packages=find_packages(),
      install_requires=['lxml','pil','pyyaml'],
)