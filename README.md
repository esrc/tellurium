Tellurium - Utility for functional testing of web sites and RESTful data sources
================================================================================

Tellurium is a utility for functional testing of web sites and RESTful
HTML/JSON/XML data sources. During development, Tellurium can be used to run
automated tests against in-house testing sites, to identify comfirm completion
of tasks and identify deficiencies. Once you're in production, run automated
tests against your production services to catch problems that may arise when
making changes to the site or associated services.

The goals of the project are:

 * Package to running service in less than 5 minutes
 * Anybody can create a basic functional test
 * Basic record keeping and reporting on historical test outcomes


Installation
------------

Tellurium is available on PyPi. Install by running the following command:

> pip install tellurium

Alternately, you can download the application package from the Bitbucket
repository:

> https://bitbucket.org/davismarques/tellurium


Usage
-----

See online documentation for usage instructions.


Credits
-------

Tellurium is a project of the eScholarship Research Center at the University
of Melbourne. For more information about the project, please contact us at:

 > eScholarship Research Center
 > University of Melbourne
 > Parkville, Victoria
 > Australia
 > www.esrc.unimelb.edu.au

 Authors:

 * Davis Marques <dmarques@unimelb.edu.au>

Thanks:

 * lxml - http://lxml.de
 * pyyaml - http://pyyaml.org
 * requests - http://python-requests.org
 * simplejson - https://github.com/simplejson/simplejson


License
-------

See the LICENSE file for copyright and license information.


Version History
---------------

Backlog:

 * Functional testing following Selenium/RobotFramework approach


Current:

0.1.0

 * YAML data testing configuration file
 * Test JSON data source
 * Test XML data source


Known Issues
------------

