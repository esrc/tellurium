"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

import logging
import lxml
import urllib2


log = logging.getLogger()


def test(url, test_conditions):
    """
    Execute tests against a JSON resource.
    """
    try:
        response = urllib2.urlopen(url)
        data = response.read()
    except:
        log.error("Could not retrieve resource {0}".format(url))

