"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

import logging
import sys
import urllib2
import utils
import the_value


__description__ = "Test runner for HTTP response object"

this_module = sys.modules[__name__]


def should_have_content(response, arg=True):
    """
    The document should not be null or have a length of zero. Returns true if
    the condition is met; false otherwise. The response object appears to not
    retain content itself, but provides a file
    """
    try:
        new_response = urllib2.urlopen(response.url)
        data = new_response.read()
        if data:
            return True
    except:
        pass
    return False

def should_have_mimetype(response, mimetype):
    """
    The document should have the specified mimetype. If more than one mimetype
    is possible for the document, a comma or double-pipe separate list should
    be provided, designating a logical or condition. Returns true if the
    condition is met; false otherwise.
    """
    info = response.info()
    if 'content-type' in info.keys():
        response_mimetype = info['content-type']
        return the_value.should_match_pattern(response_mimetype, mimetype)
    return False

def should_have_status_code(response, arg):
    """
    The HTTP status code is a three chracter numeric identifier used to
    specify the outcome of an HTTP request. Determine whether the status code
    matches an expected value. See the HTTP Specification for a list of
    response codes.

    Specify the response code or codes to be matches as a regex pattern. See
    the Python documentation manual for a description of the re module.

    Some examples include:

     * Match successful HTTP requests: 200, 2..
     * Match server errors: 5..
     * Match document not found: 404
    """
    code = response.getcode()
    return the_value.should_match_pattern(code, arg)

def test(response, tests):
    """
    Execute tests against the response object.
    """
    results = []
    for test_name, test_condition in [t.items().pop() for t in tests]:
        try:
            func_name, func_arg = utils.get_test_name_and_argument(test_name)
            f = getattr(this_module, func_name)
            r = f(response, test_condition)
            result = {test_name: r}
            results.append(result)
            logging.info("the_response {} {}: {}".format(func_name, test_condition, r))
        except:
            logging.error("Exception in test {0}".format(test_name), exc_info=True)
    return results

