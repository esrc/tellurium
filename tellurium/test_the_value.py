"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

import the_value
import unittest


__description__ = "Unit tests for the_value module"


class TestUtils(unittest.TestCase):
    """
    Unit tests for the_field module.
    """

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_should_be_equal_to(self):
        """
        It should return true when two integer values are equal. In all other
        cases it should return false.
        """
        cases = [
            (1, 1, True),
            (1, 2, False),
            (-1, -1, True),
            (1.0, 1, True),
            ('1', '1', True),
            ('a', 1, False),
            (1, 'a', False),
            ('a','b', False),
            (None, 1, False),
            (1, None, False),
            (None, None, False),
        ]
        for case in cases:
            a, b, expected = case
            result = the_value.should_be_equal_to(a, b)
            self.assertNotEqual(result, None)
            self.assertEqual(result, expected)

    def test_should_be_greater_than(self):
        """
        It should return true when integer value A is greater than integer
        value B. It should return false in all other cases.
        """
        cases = [
            (1, 1, False),
            (2, 1, True),
            (1, 2, False),
            ('a', 1, False),
            (1, 'a', False),
            ('1', '0', True),
        ]
        for case in cases:
            a, b, expected = case
            result = the_value.should_be_greater_than(a, b)
            self.assertNotEqual(result, None)
            self.assertEqual(result, expected)

    def test_should_be_greater_than_or_equal(self):
        """
        It should return true when integer value A is greater than or equal to
        the integer value B. It should return false in all other cases.
        """
        cases = [
            (1, 1, True),
            (1, 0, True),
            (0, 1, False),
            (1, -1, True),
            ('a', 'b', False),
            ('1', '1', True),
            ('2', '1', True),
        ]
        for case in cases:
            a, b, expected = case
            result = the_value.should_be_greater_than_or_equal(a, b)
            self.assertNotEqual(result, None)
            self.assertEqual(result, expected)

    def test_should_be_less_than(self):
        """
        It should return true when integer value A is less than integer
        value B. It should return false in all other cases.
        """
        cases = [
            (1, 1, False),
            (1, 0, False),
            (0, 1, True),
            (-1, 1, True),
            ('a', 'b', False),
            ('1', '1', False),
            ('1', '2', True),
        ]
        for case in cases:
            a, b, expected = case
            result = the_value.should_be_less_than(a, b)
            self.assertNotEqual(result, None)
            self.assertEqual(result, expected)

    def test_should_be_less_than_or_equal(self):
        """
        It should return true when integer value A is less than or equal to the
        integer value B. It should return false in all other cases.
        """
        cases = [
            (1, 1, True),
            (1, 0, False),
            (0, 1, True),
            (-1, 1, True),
            ('a', 'b', False),
            ('1', '1', True),
            ('1', '2', True),
        ]
        for case in cases:
            a, b, expected = case
            result = the_value.should_be_less_than_or_equal(a, b)
            self.assertNotEqual(result, None)
            self.assertEqual(result, expected)

    def test_should_match_pattern(self):
        """
        It should return true when the string includes a substring that matches
        the specified regex pattern. It should return False in all other cases.
        """
        cases = [
        ]
        for case in cases:
            value, pattern, expected = case
            result = the_value.should_match_pattern(value, pattern)
            self.assertNotEqual(result, None)
            self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main()
