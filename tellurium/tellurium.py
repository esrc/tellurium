"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

import ConfigParser
import argparse
import datetime
import importlib
import logging
import os
import sys
import the_action
import utils
import yaml


class Tellurium(object):
    """
    A utility for executing functional tests against a web resource.
    """

    def __init__(self):
        """
        Configure command parser and logging.
        """
        self.config = ConfigParser.SafeConfigParser()
        self.log = logging.getLogger()
        self.parser = argparse.ArgumentParser(description=self.__doc__)
        self.parser.add_argument('-t', '--test', help="Test configuration")
        self.parser.add_argument('-l', '--loglevel', help="Set the logging level", choices=['DEBUG','INFO','ERROR'])

    def configure_logging(self):
        """
        Configure logging with console stream handler.
        """
        # formatter = logging.Formatter("%(asctime)s - %(filename)s %(lineno)03d - %(levelname)s - %(message)s")
        formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
        sh = logging.StreamHandler(sys.stdout)
        sh.setFormatter(formatter)
        # set the logging level on both the logger and the handler
        if self.args.loglevel and self.args.loglevel == 'DEBUG':
            level = logging.DEBUG
        elif self.args.loglevel and self.args.loglevel == 'INFO':
            level = logging.INFO
        else:
            level = logging.ERROR
        sh.setLevel(level)
        self.log.addHandler(sh)
        self.log.setLevel(level)

    def print_time(self, delta):
        """
        Print a summary of the elapsed time to the console.
        """
        s = delta.seconds
        hours, remainder = divmod(s, 3600)
        minutes, seconds = divmod(remainder, 60)
        self.log.info('Test finished in {0}:{1}:{2}'.format(hours, minutes, seconds))

    def run(self):
        """
        Start processing.
        """
        # parse the command line arguments and set logging
        try:
            self.args = self.parser.parse_args()
            self.configure_logging()
            self.log.info("Running {0}".format(' '.join(sys.argv[1:])))
        except Exception as e:
            self.parser.print_help()
            sys.exit()
        # load the configuration file
        try:
            if not self.args.test:
                self.parser.print_help()
                sys.exit()
            else:
                with open(self.args.test, 'r') as f:
                    self.test_spec = yaml.load(f)
        except Exception as e:
            self.log.critical("Could not load the specified configuration file")
            sys.exit(e)
        # execute testing jobs
        start = datetime.datetime.now()
        test_results = self.test()
        delta = datetime.datetime.now() - start
        self.print_time(delta)
        # handle test results
        filename = self.args.test.split(os.sep)[-1]
        the_action.handle(filename, self.test_spec, test_results)

    def test(self):
        """
        Call the specified functions to execute the tests. The first line of
        the test case specifies the name of the testing module and the URL
        for the resource being tested.
        """
        # defaults
        definitions = {} if not self.test_spec['define'] else self.test_spec['define']
        test_spec = {} if not self.test_spec['tests'] else self.test_spec['tests']
        # execute test cases
        results = []
        for test_case in test_spec:
            for label, tests in test_case.items():
                module_name, arg = utils.get_test_name_and_argument(label)
                try:
                    module = importlib.import_module(module_name)
                    func = getattr(module, 'test')
                    result = func(arg, tests, definitions)
                    results.append(result)
                except:
                    self.log.error("Test failed when executing {0}".format(module_name), exc_info=True)
        return results


if __name__ == '__main__':
    t = Tellurium()
    t.run()
