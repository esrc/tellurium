"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

import unittest
import utils


class TestTellurium(unittest.TestCase):
    """
    Unit tests for tellurium module.
    """

    def setUp(self):
        pass

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main()