"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import logging
import smtplib
import sys


__description__ = "Test result handling actions"

this_module = sys.modules[__name__]


def count_failed_tests(test_results):
    """
    Get the number of failed tests from the test results.
    """
    count = 0
    results = get_test_results_as_flat_list(test_results)
    for k, v in [d.items().pop() for d in results]:
        count = count + 1 if not v else count
    return count

def count_successful_tests(test_results):
    """
    Get the number of successful tests from the test results.
    """
    count = 0
    results = get_test_results_as_flat_list(test_results)
    for k, v in [d.items().pop() for d in results]:
        count = count + 1 if v else count
    return count

def get_test_results_as_flat_list(test_results):
    """
    Get a flat list of test results from the test_results object.
    """
    items = []
    try:
        if isinstance(test_results, dict):
            for k,v in test_results.items():
                if isinstance(v, list):
                    result = get_test_results_as_flat_list(v)
                    items.extend(result)
                else:
                    item = {k:v}
                    items.append(item)
        elif isinstance(test_results, list):
            for item in test_results:
                result = get_test_results_as_flat_list(item)
                items.extend(result)
    except:
        pass
    return items

def get_test_results_as_string(test_results):
    """
    Get test results as a formatted string.
    """
    results = get_test_results_as_flat_list(test_results)
    s = ""
    for k, v in [d.items().pop() for d in results]:
        s += "{{{0}: {1}}}\n".format(k, v)
    return s

def send_report(recipients, test_results, subject="Test results", project="Project"):
    """
    Send test report by email.
    """
    msg_from = "tellurium-test-runner@esrc.unimelb.edu.au"
    msg_results = get_test_results_as_string(test_results)
    try:
        msg = MIMEMultipart('alternative')
        # message headers
        msg['Subject'] = "Test results for {0}".format(project)
        msg['From'] = msg_from
        msg['To'] = recipients
        # message content
        plain = "{0}\n\n----\n\nTest Results:\n\n{1}".format("General message", msg_results)
        html = "{0}<hr/><p>Test Results:</p><pre>{1}</pre>".format("General message", msg_results)
        part1 = MIMEText(plain, 'plain')
        # part2 = MIMEText(html, 'html')
        msg.attach(part1)
        # msg.attach(part2)
        # send email to recipient
        s = smtplib.SMTP('localhost')
        s.sendmail(msg_from, recipients, msg.as_string())
        return True
    except:
        msg = "Could not send '{0}' report to {1}".format(project, recipients)
        logging.error(msg, exc_info=True)
        return False

def handle_actions(project, actions, test_results):
    """
    Handle result actions.
    """
    for func_name, func_arg in [d.items().pop() for d in actions]:
        try:
            f = getattr(this_module, func_name)
            if 'send_report' in func_name:
                r = f(func_arg, test_results, project=project)
            else:
                r = f(func_arg, test_results)
            logging.info("the_action {} {}: {}".format(func_name, func_arg, r))
        except:
            logging.error("Could not complete action {0}".format(func_name), exc_info=True)

def handle(project, rules, test_results):
    """
    Execute handling rules for test_results.
    """
    if 'on_error' in rules and rules['on_error'] != None and count_failed_tests(test_results) > 0:
        actions = rules['on_error']
        handle_actions(project, actions, test_results)
    if 'on_success' in rules and rules['on_success'] != None:
        actions = rules['on_success']
        handle_actions(project, actions, test_results)
