"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

import os
import sys
import the_action
import unittest
import yaml


__description__ = "Unit tests for the_result module"

this_module = sys.modules[__name__]
this_module_path = os.path.dirname(__file__)


class Test_the_action(unittest.TestCase):
    """
    Unit test for the_result module.
    """

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_count_failed_tests(self):
        """
        It should return a count of the number of failed tests cases identified
        in the test results.
        """
        cases = [
            ("{0}/test_cases/test_results_1.yml".format(this_module_path), 1),
            ("{0}/test_cases/test_results_2.yml".format(this_module_path), 0),
            ("{0}/test_cases/test_results_3.yml".format(this_module_path), 7),
        ]
        for case in cases:
            path, expected = case
            with open(path, 'r') as f:
                data = yaml.load(f.read())
            result = the_action.count_failed_tests(data)
            self.assertNotEqual(result, None)
            self.assertEqual(result, expected)

    def test_count_successful_tests(self):
        """
        It should return a count of the number of successful test cases
        identified in the test results.
        """
        cases = [
            ("{0}/test_cases/test_results_1.yml".format(this_module_path), 6),
            ("{0}/test_cases/test_results_2.yml".format(this_module_path), 7),
            ("{0}/test_cases/test_results_3.yml".format(this_module_path), 0),
        ]
        for case in cases:
            path, expected = case
            with open(path, 'r') as f:
                data = yaml.load(f.read())
            result = the_action.count_successful_tests(data)
            self.assertNotEqual(result, None)
            self.assertEqual(result, expected)

    def test_get_test_results_as_flat_list(self):
        """
        It should return a flat list of test results.
        """
        cases = [
            ("{0}/test_cases/test_results_1.yml".format(this_module_path), 7),
            ("{0}/test_cases/test_results_2.yml".format(this_module_path), 7),
            ("{0}/test_cases/test_results_3.yml".format(this_module_path), 7),
        ]
        for case in cases:
            path, expected = case
            with open(path, 'r') as f:
                data = yaml.load(f.read())
            result = the_action.get_test_results_as_flat_list(data)
            self.assertNotEqual(result, None)
            self.assertEqual(len(result), expected)

    def test_get_test_results_as_string(self):
        """
        It should return the test results as a formatted string.
        """
        cases = [
            ("{0}/test_cases/test_results_1.yml".format(this_module_path), 240),
            ("{0}/test_cases/test_results_2.yml".format(this_module_path), 240),
            ("{0}/test_cases/test_results_3.yml".format(this_module_path), 240),
        ]
        for case in cases:
            path, expected = case
            with open(path, 'r') as f:
                data = yaml.load(f.read())
            result = the_action.get_test_results_as_string(data)
            self.assertNotEqual(result, None)
            self.assertGreater(len(result), expected)

    def test_send_report(self):
        """
        It should send an email message with
        """
        cases = []
        for case in cases:
            test_results, expected = case
            result = the_action.count_failed_tests(test_results)
            self.assertNotEqual(result, None)
            self.assertEqual(result, expected)
