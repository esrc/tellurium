"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

__author__ = 'Davis Marques <dmarques@unimelb.edu.au>'
__description__ = "Utility for functional testing of web resources"
