"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

import json
import os
import the_json
import unittest


this_module_path = os.path.dirname(__file__)


class TestJsonArray(unittest.TestCase):
    """
    Unit test for JsonArray class.
    """

    def setUp(self):
        path = "{0}/test_cases/json_array_1.jsn".format(this_module_path)
        with open(path, 'r') as f:
            self.data = f.read()
            self.json = json.loads(self.data)
        self.definitions = {"var":"TARGET"}
        self.array = the_json.JsonArray(self.json, [], self.definitions)

    def tearDown(self):
        pass

    def test_should_have_member(self):
        """
        It should return true if the array contains one or more members that
        match the specified properties. It should return false in all other
        cases.
        """
        cases = [
            ({"id":"abc"}, True),
            ({"id":"_abc"}, False),
            ({"id":"SE004920"}, True),
            ({"id":"SE004920","localtype":"Archival Series"}, True),
            ({"id":"SE004920","localtype":"Series"}, False),
            ({"id":"TD0000020","localtype":"Organisation"}, True),
            ({"id":"TD0000020","localtype":"None"}, False),
            ({"id":"TARGET"}, True),
        ]
        for case in cases:
            match_pattern, expected = case
            result = self.array.should_have_member([match_pattern])
            self.assertNotEqual(result, None)
            self.assertEqual(result, expected)

    def test_size_equals(self):
        """
        It should return true if the array length equals the specified size.
        It should return false in all other cases.
        """
        cases = [
            (2, False),
            (9, True),
            (10, False),
        ]
        for case in cases:
            size, expected = case
            result = self.array.size_equals(size)
            self.assertNotEqual(result, None)
            self.assertEqual(result, expected)

    def test_size_greater_than(self):
        """
        It should return true if the array length is greater than the
        specified size. It should return false in all other cases.
        """
        cases = [
            (3, True),
            (8, True),
            (9, False),
            (10, False),
        ]
        for case in cases:
            size, expected = case
            result = self.array.size_greater_than(size)
            self.assertNotEqual(result, None)
            self.assertEqual(result, expected)

    def test_size_greater_than_or_equal(self):
        """
        It should return true if the array length is greater than or equal to
        the specified size. It should return false in all other cases.
        """
        cases = [
            (3, True),
            (8, True),
            (9, True),
            (10, False),
        ]
        for case in cases:
            size, expected = case
            result = self.array.size_greater_than_or_equal(size)
            self.assertNotEqual(result, None)
            self.assertEqual(result, expected)

    def test_size_less_than(self):
        """
        It should return true if the array length is less than the specified
        size. It should return false in all other cases.
        """
        cases = [
            (3, False),
            (8, False),
            (9, False),
            (10, True),
        ]
        for case in cases:
            size, expected = case
            result = self.array.size_less_than(size)
            self.assertNotEqual(result, None)
            self.assertEqual(result, expected)

    def test_size_less_than_or_equal(self):
        """
        It should return true if the array length is less than or equal to the
        specified size. It should return false in all other cases.
        """
        cases = [
            (3, False),
            (8, False),
            (9, True),
            (10, True),
        ]
        for case in cases:
            size, expected = case
            result = self.array.size_less_than_equal(size)
            self.assertNotEqual(result, None)
            self.assertEqual(result, expected)


class TestJsonField(unittest.TestCase):
    """
    Unit tests for JsonField class.
    """

    def setUp(self):
        path = "{0}/test_cases/json_field_1.jsn".format(this_module_path)
        with open(path, 'r') as f:
            self.data = f.read()
            self.json = json.loads(self.data)
        self.definitions = {"var":"TARGET"}
        self.field = the_json.JsonField(self.json, None, self.definitions)

    def tearDown(self):
        pass


class TestJsonObject(unittest.TestCase):
    """
    Unit tests for JsonObject class.
    """

    def setUp(self):
        path = "{0}/test_cases/json_object_1.jsn".format(this_module_path)
        with open(path, 'r') as f:
            self.data = f.read()
            self.json = json.loads(self.data)
        self.definitions = {"var": "TARGET"}
        self.object = the_json.JsonObject(self.json, None, self.definitions)

    def tearDown(self):
        pass


class Test_the_json(unittest.TestCase):
    """
    Unit tests for the_json module.
    """

    def setUp(self):
        pass

    def tearDown(self):
        pass


if __name__ == '__main__':
    unittest.main()