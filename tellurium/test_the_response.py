"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

import the_response
import unittest
import urllib2


__description__ = "Test cases for the_response module"


class Test_the_response(unittest.TestCase):
    """
    Unit tests for the_response module.
    """

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_should_have_content(self):
        """
        It should return true if the response has a content payload.
        """
        cases = [
            ('http://data.esrc.unimelb.edu.au/solr/FACP/select?q=*:*&rows=2&sort=title+asc&wt=json', True),
            ('http://www.findandconnect.gov.au/', True),
            ('http://www.findandconnect.au/', False),
        ]
        for case in cases:
            url, expected = case
            try:
                response = urllib2.urlopen(url)
                result = the_response.should_have_content(response)
                self.assertNotEqual(result, None)
                self.assertEqual(result, expected)
            except:
                self.assertEqual(False, expected)

    def test_should_have_mimetype(self):
        """
        It should return true if the response mimetype is matched by the
        specified pattern. It should return false in all other cases.
        """
        cases = [
            ('http://data.esrc.unimelb.edu.au/solr/FACP/select?q=*:*&rows=2&sort=title+asc&wt=json', 'application/json', True),
            ('http://data.esrc.unimelb.edu.au/solr/FACP/select?q=*:*&rows=2&sort=title+asc&wt=json', 'text/json', False),
            ('http://data.esrc.unimelb.edu.au/solr/FACP/select?q=*:*&rows=2&sort=title+asc&wt=json', 'application/json|text/json', True),
            ('http://data.esrc.unimelb.edu.au/solr/FACP/select?q=*:*&rows=2&sort=title+asc&wt=json', 'text/html', False),
            ('http://data.esrc.unimelb.edu.au/solr/FACP/select?q=*:*&rows=2&sort=title+asc&wt=xml', 'application/xml', True),
            ('http://data.esrc.unimelb.edu.au/solr/FACP/select?q=*:*&rows=2&sort=title+asc&wt=xml', 'application/xml|text/plain', True),
            ('http://data.esrc.unimelb.edu.au/solr/FACP/select?q=*:*&rows=2&sort=title+asc&wt=xml', 'text/plain', False),
            ('http://www.findandconnect.gov.au/', 'text/html', True),
            ('http://www.findandconnect.gov.au/', 'application/json', False),
            ('http://www.findandconnect.gov.au/', 'text/html|application/json', True),
        ]
        for case in cases:
            url, match_pattern, expected = case
            response = urllib2.urlopen(url)
            result = the_response.should_have_mimetype(response, match_pattern)
            self.assertNotEqual(result, None)
            self.assertEqual(result, expected)

    def test_should_have_status_code(self):
        """
        It should return true when the HTTP response code matches the specified
        pattern. It should return false otherwise.
        """
        cases = [
            ('http://www.findandconnect.gov.au/', '200', True),
            ('http://www.findandconnect.gov.au/', '2..', True),
            ('http://www.findandconnect.gov.au/', '...', True),
            ('http://www.findandconnect.gov.au/', '2[0-9]{2}', True),
            ('http://www.findandconnect.au/', None, False),
        ]
        for case in cases:
            url, match_pattern, expected = case
            try:
                response = urllib2.urlopen(url)
                result = the_response.should_have_status_code(response, match_pattern)
                self.assertNotEqual(result, None)
                self.assertEqual(result, expected)
            except:
                self.assertEqual(False, expected)

    def test_should_not_be_empty(self):
        """
        It should return true when the document contains data, false otherwise.
        """
        cases = [
            ('http://data.esrc.unimelb.edu.au/solr/FACP/select?q=*:*&rows=2&sort=title+asc&wt=json', True),
            ('http://www.findandconnect.gov.au/', True),
            ('http://www.findandconnect.au/', False),
        ]
        for case in cases:
            url, expected = case
            try:
                response = urllib2.urlopen(url)
                result = the_response.should_have_content(response)
                self.assertNotEqual(result, None)
                self.assertEqual(result, expected)
            except:
                self.assertEqual(False, expected)

