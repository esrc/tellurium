"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

from types import ListType

import Image
import logging
import urllib2
import utils


log = logging.getLogger()


class ImageResource(object):
    """
    Test runner for image resource.
    """

    def __init__(self, source, tests, definitions):
        assert type(tests) is ListType
        self.tests = tests
        self.definitions = definitions
        self.source = utils.strip_quotes(source)
        self.source = utils.replace_variables(self.source, definitions)
        # retrieve the data to be tested
        if utils.is_file_resource(self.source):
            with open(self.source, 'r') as f:
                self.response = None
                self.data = f.read()
        else:
            self.response = urllib2.urlopen(self.source)
            self.data = self.response.read()
        # self.image = json.loads(self.data)

    def test(self):
        """
        Execute tests on the JSON data source.
        """
        logging.info("the_image {0}".format(self.source))
        # j = JsonObject(self.json, self.tests, self.definitions, name=self.source, response=self.response)
        return {} #{"the_json {0}".format(self.source): j.test()}


def test(source, tests, definitions):
    """
    Execute tests against a JSON resource.
    """
    try:
        r = ImageResource(source, tests, definitions)
        return r.test()
    except:
        msg = "Could not retrieve resource {0}".format(source)
        logging.error(msg, exc_info=False)
        return {msg: False}
