"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

from lxml import etree
from types import ListType

import logging
import sys
import the_response
import the_value
import urllib2
import utils


__description__ = "Test runner for XML resource"

exc_info = True
this_module = sys.modules[__name__]


class XMLAttribute(object):
    """
    Test runner for XML element.
    """

    def __init__(self, data, tests, definitions, name=None):
        self.data = data
        self.definitions = definitions
        self.name = name
        self.tests = tests

    def test(self):
        """
        Execute tests against the resource.
        """
        results = []
        for func_name, func_arg in [test.items().pop() for test in self.tests]:
            try:
                f = getattr(self, func_name)
                r = f(func_arg)
                result = {"the_attribute {0}".format(func_name): r}
                logging.info("the_attribute {} {} {}: {}".format(self.name, func_name, func_arg, r))
            except:
                result = {"the_attribute {0}".format(func_name): False}
                logging.error("the_attribute {} {} {}: {}".format(self.name, func_name, func_arg, False), exc_info=exc_info)
            finally:
                results.append(result)
        return results


class XMLElement(object):
    """
    Test runner for XML element.
    """

    def __init__(self, element, tests, definitions, name=None, namespace=None):
        assert type(tests) is ListType
        self.definitions = definitions
        self.element = element
        self.name = name
        self.tests = tests

    def test(self):
        """
        Execute tests against the resource.
        """
        logging.info("the_element {}".format(self.name))
        results = []
        for func_name, test_spec in [test.items().pop() for test in self.tests]:
            func_arg = None
            try:
                if func_name.startswith('the_attribute'):
                    pass
                elif func_name.startswith('the_value'):
                    t = self.element.pop().text if isinstance(self.element, list) else self.element.text
                    r = the_value.test(t, test_spec)
                else:
                    f = getattr(self, func_name)
                    r = f(func_arg)
                result = {"{} {}".format(func_name, func_arg): r} if func_arg else {"{}".format(func_name): r}
            except:
                result = {"{} {}".format(func_name, func_arg): False} if func_arg else {"{}".format(func_name): False}
                logging.error("the_element {} {} {}: {}".format(self.name, func_name, func_arg, False), exc_info=exc_info)
            finally:
                results.append(result)
        return results


class XmlResource(object):
    """
    Test runner for XML resource.
    """

    def __init__(self, source, tests, definitions):
        assert type(tests) is ListType
        self.definitions = definitions
        self.namespaces = {}
        self.source = source
        self.tests = tests
        # retrieve the data to be tested
        if utils.is_file_resource(self.source):
            with open(self.source, 'r') as f:
                self.response = None
                self.data = f.read()
        else:
            self.response = urllib2.urlopen(self.source)
            self.data = self.response.read()

    def test(self):
        """
        Execute tests on the JSON data source.
        """
        logging.info("the_xml {}".format(self.source))
        self.xml = etree.fromstring(self.data)
        results = []
        for func_name, test_spec in [test.items().pop() for test in self.tests]:
            try:
                func_name, func_arg = utils.get_test_name_and_argument(func_name)
                if func_name.startswith('the_response'):
                    r = the_response.test(self.response, test_spec)
                elif func_name.startswith('with_namespace'):
                    r = self.with_namespaces(test_spec)
                else:
                    func_arg = utils.replace_variables(func_arg, self.definitions)
                    if func_name.startswith('the_attribute'):
                        obj = XMLAttribute(self.xml[func_arg], test_spec, self.definitions, name=func_arg)
                        r = obj.test()
                    elif func_name.startswith('the_element'):
                        e = self.xml.xpath(func_arg, namespaces=self.namespaces)
                        obj = XMLElement(e, test_spec, self.definitions, name=func_arg)
                        r = obj.test()
                    else:
                        f = getattr(self, func_name)
                        r = f(func_arg, test_spec)
                result = {"{} {}".format(func_name, func_arg): r} if func_arg else {func_name: r}
                results.append(result)
            except:
                logging.error("Exception in test {}".format(func_name), exc_info=exc_info)
                results.append({func_name: False})
        return results

    def with_namespaces(self, namespaces):
        """
        Use the specified namespace when executing queries.
        """
        for k, v in [d.items().pop() for d in namespaces]:
            self.namespaces[k] = v
        return True


def test(source, tests, definitions):
    """
    Execute tests against an XML resource.
    """
    assert type(tests) is ListType
    try:
        source = utils.strip_quotes(source)
        source = utils.replace_variables(source, definitions)
        x = XmlResource(source, tests, definitions)
        return x.test()
    except:
        msg = "Could not retrieve resource {0}".format(source)
        logging.error(msg, exc_info=exc_info)
        return {msg: False}
