"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

from types import ListType

import logging
import re
import sys


__description__ = "Functions used to determine whether a value conforms to a requirement"

log = logging.getLogger()
this_module = sys.modules[__name__]

def should_be_equal_to(A, B):
    """
    Determine if the integer A is equal to the integer B.
    """
    try:
        if int(A) == int(B):
            return True
    except:
        pass
    return False

def should_be_greater_than(A, B):
    """
    Determine if the integer A is greater than the integer B.
    """
    try:
        if int(A) > int(B):
            return True
    except:
        pass
    return False

def should_be_greater_than_or_equal(A, B):
    """
    Determine if the integer A is greater than or equal to the integer
    B.
    """
    try:
        if int(A) >= int(B):
            return True
    except:
        pass
    return False

def should_be_less_than(A, B):
    """
    Determine if the integer A is less than the integer B.
    """
    try:
        if int(A) < int(B):
            return True
    except:
        pass
    return False

def should_be_less_than_or_equal(A, B):
    """
    Determine if the integer A is less than or equal to the integer B.
    """
    try:
        if int(A) <= int(B):
            return True
    except:
        pass
    return False

def should_match_pattern(s, pat):
    """
    Determine if the string matches the specified pattern.
    """
    r = re.compile(str(pat))
    if r.search(str(s)):
        return True
    return False

def should_not_be_empty(s, val):
    """
    Determine if the string s is empty.
    """
    return True if s and val else False

def test(test_value, test_spec):
    """
    Execute the specified tests against the test value.
    """
    assert type(test_spec) is ListType
    results = []
    for test_name, reference_value in [test.items().pop() for test in test_spec]:
        try:
            f = getattr(this_module, test_name)
            r = f(test_value, reference_value)
            result = {test_name: r}
            results.append(result)
            logging.info("the_value {} {}: {}".format(test_name, reference_value, r))
        except:
            log.error("Exception in test {0}".format(test_name), exc_info=True)
    return results
