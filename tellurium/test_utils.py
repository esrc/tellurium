"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

import unittest
import utils


class TestUtils(unittest.TestCase):
    """
    Unit tests for utils module.
    """

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_get_test_case_label_and_argument(self):
        """
        It should split the test case label at the first space character. If no
        space character is present, return an empty string.
        """
        cases = [
            ('abc def',('abc','def')),
            ('abc def xyz',('abc','def xyz')),
            ('abc',('abc',None))
        ]
        for case in cases:
            value, expected = case
            result = utils.get_test_name_and_argument(value)
            self.assertNotEqual(result, None)
            self.assertEqual(result, expected)

    def test_replace_variables(self):
        """
        It should replace all variables that are identified by the pattern
        ${variable_name}.
        """
        replacement_map = {
            'abc': 'def',
            'xyz': 'zzz'
        }
        cases = [
            ('this is a ${abc} jam','this is a def jam'),
            ('this ${xyz} has a ${abc} in it','this zzz has a def in it'),
        ]
        for case in cases:
            value, expected = case
            result = utils.replace_variables(value, replacement_map)
            self.assertNotEqual(result, None)
            self.assertEqual(result, expected)

    def test_strip_quotes(self):
        """
        It should remove leading and trailing single and double quotation marks
        from the provided string.
        """
        cases = [
            ("abc","abc"),
            ("""\"abc\"""","abc"),
            ("""\'abc\'""","abc"),
            ("""'abc'""","abc"),
            ('''"abc"''',"abc"),
        ]
        for case in cases:
            value, expected = case
            result = utils.strip_quotes(value)
            self.assertNotEqual(result, None)
            self.assertEqual(result, expected)


if __name__ == '__main__':
    unittest.main()

