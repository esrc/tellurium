"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""


def get_test_name_and_argument(S):
    """
    Extract the argument value from the test case label. The argument is the
    text that follows the first space character.
    """
    i = S.find(' ')
    if i != -1:
        label = S[:i]
        arg = S[i+1:]
        return (label, arg)
    return S, None

def is_file_resource(url):
    """
    Determine if the URL represents a file system resource. If the URL
    does not represent a file system resource, we assume that it represents
    a web accessible resource.
    """
    if not url.startswith('http:') and not url.startswith('https:'):
        return True
    return False

def replace_variables(S, replacements):
    """
    Replace variable placeholders in the string S.
    """
    for var, replacement in replacements.items():
        target = "${{{0}}}".format(var)
        S = S.replace(target, replacement)
    return S

def strip_quotes(S):
    """
    String starting and ending quotation marks from the string.
    """
    if S[0] == "\"" or S[0] == "'":
        S = S[1:]
    if S[-1] == "\"" or S[-1] == "'":
        S = S[0:-1]
    return S
