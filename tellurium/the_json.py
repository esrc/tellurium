"""
This file is subject to the terms and conditions defined in the
LICENSE file, which is part of this source code package.
"""

from types import ListType

import json
import logging
import sys
import the_value
import the_response
import urllib2
import utils


__description__ = "Test runner for JSON resource"

exc_info = False
this_module = sys.modules[__name__]


class JsonArray(object):
    """
    Test runner for JSON array data structure.
    """

    def __init__(self, json, tests, definitions, name=None):
        assert type(tests) is ListType
        self.definitions = definitions
        self.json = json
        self.name = name
        self.tests = tests

    def should_have_member(self, cases):
        """
        Determine whether the array contains a member with the specified
        properties.
        """
        case_matches = 0
        for case in cases:
            for member in self.json:
                member_match = 0
                for k, v in case.items():
                    if k in member:
                        member_val = utils.replace_variables(member[k], self.definitions)
                        member_match = member_match + 1 if member_val == v else member_match
                if member_match == len(case.items()):
                    case_matches += 1
                    break
        return True if case_matches == len(cases) else False

    def size_equals(self, val):
        """
        Determine if the size of the array is equal to the specified value.
        """
        if len(self.json) == val:
            return True
        return False

    def size_greater_than(self, val):
        """
        Determine if the size of the array is greater than the specified
        value.
        """
        if len(self.json) > val:
            return True
        return False

    def size_greater_than_or_equal(self, val):
        """
        Determine if the size of the array is greater than or equal to the
        specified value.
        """
        if len(self.json) >= val:
            return True
        return False

    def size_less_than(self, val):
        """
        Determine if the size of the array is less than the specified
        value.
        """
        if len(self.json) < val:
            return True
        return False

    def size_less_than_equal(self, val):
        """
        Determine if the size of the array is less than or equal to the
        specified value.
        """
        if len(self.json) <= val:
            return True
        return False

    def test(self):
        """
        Execute tests against the resource.
        """
        results = []
        for func_name, func_arg in [test.items().pop() for test in self.tests]:
            try:
                f = getattr(self, func_name)
                r = f(func_arg)
                result = {"the_array {0}".format(func_name): r}
                logging.info("the_array {} {} {}: {}".format(self.name, func_name, func_arg, r))
            except:
                result = {"the_array {0}".format(func_name): False}
                logging.error("the_array {} {} {}: {}".format(self.name, func_name, func_arg, False), exc_info=exc_info)
            finally:
                results.append(result)
        return results


class JsonField(object):
    """
    Test runner for JSON field data structure.
    """

    def __init__(self, field_value, tests, definitions, name=None):
        assert type(tests) is ListType
        self.definitions = definitions
        self.field_value = field_value
        self.name = name
        self.tests = tests

    def test(self):
        """
        Execute tests against the data structure.
        """
        results = []
        for func_name, func_arg in [test.items().pop() for test in self.tests]:
            try:
                f = getattr(the_value, func_name)
                r = f(self.field_value, func_arg)
                result = {"the_field {0}".format(func_name): r}
                logging.info("the_field {} {} {}: {}".format(self.name, func_name, func_arg, r))
            except:
                result = {"the_field {0}".format(func_name): False}
                logging.error("the_field {} {} {}: {}".format(self.name, func_name, func_arg, False), exc_info=exc_info)
            finally:
                results.append(result)
        return results


class JsonObject(object):
    """
    Test runner for JSON object data structure.
    """

    def __init__(self, obj, tests, definitions, name=None, response=None):
        assert type(tests) is ListType
        self.definitions = definitions
        self.json = obj
        self.name = name
        self.tests = tests
        if response:
            self.response = response

    def test(self):
        """
        Execute tests against JSON object. Return test results as a list of
        test name, test result tuples.
        """
        logging.info("the_object {0}".format(self.name))
        results = []
        func_arg = None
        for func_name, test_spec in [test.items().pop() for test in self.tests]:
            try:
                obj = r = None
                func_name, func_arg = utils.get_test_name_and_argument(func_name)
                if func_name.startswith('the_response'):
                    r = the_response.test(self.response, test_spec)
                else:
                    func_arg = utils.replace_variables(func_arg, self.definitions)
                    if func_name.startswith('the_array'):
                        obj = JsonArray(self.json[func_arg], test_spec, self.definitions, name=func_arg)
                    elif func_name.startswith('the_field'):
                        obj = JsonField(self.json[func_arg], test_spec, self.definitions, name=func_arg)
                    elif func_name.startswith('the_object'):
                        obj = JsonObject(self.json[func_arg], test_spec, self.definitions, name=func_arg)
                    if obj:
                        r = obj.test()
                    else:
                        f = getattr(self, func_name)
                        r = f(func_arg, test_spec)
                if r:
                    result = {"{} {}".format(func_name, func_arg): r} if func_arg else {func_name: r}
            except:
                result = {"the_object {} {}".format(func_name, func_arg): False} if func_arg else {func_name: False}
            finally:
                results.append(result)
        return results


class JsonResource(object):
    """
    Test runner for JSON data source.
    """

    def __init__(self, source, tests, definitions):
        assert type(tests) is ListType
        self.definitions = definitions
        self.source = source
        self.tests = tests
        # retrieve the data to be tested
        if utils.is_file_resource(self.source):
            with open(self.source, 'r') as f:
                self.response = None
                self.data = f.read()
        else:
            self.response = urllib2.urlopen(self.source)
            self.data = self.response.read()

    def test(self):
        """
        Execute tests on the JSON data source.
        """
        logging.info("the_json {0}".format(self.source))
        self.json = json.loads(self.data)
        j = JsonObject(self.json, self.tests, self.definitions, name=self.source, response=self.response)
        return {"the_json {0}".format(self.source): j.test()}


def test(source, tests, definitions):
    """
    Execute tests against a JSON resource.
    """
    try:
        source = utils.strip_quotes(source)
        source = utils.replace_variables(source, definitions)
        r = JsonResource(source, tests, definitions)
        return r.test()
    except:
        msg = "Could not retrieve resource {0}".format(source)
        logging.error(msg, exc_info=exc_info)
        return {msg: False}
